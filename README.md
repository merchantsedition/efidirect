
# EFIdirect

**EFIdirect** leverages bootloader capabilities built into (U)EFI. About every
modern x86-based computer uses some flavor of EFI as its BIOS. It can replace,
but doesn't conflict with secondary bootloaders like Grub, Lilo or rEFInd.


## How it works

EFIdirect comes with two scripts. One gets triggered on installation of a
kernel, the other one on removal of a kernel. On installation of a kernel,
EFIdirect copies the just installed kernel, along with its `initrd.img`, into
the EFI System Partition and writes entries for these into the EFI bootloader.

On kernel uninstallation, its copy and its EFI bootloader entries get removed.

EFIdirect works automatically in the background, without manual interaction.


## Installation

Installation is fairly simple.

### Installing the Distribution Package

Not yet, a Debian package is work in progress.

### Manual Installation

 1. Grab a copy of this Git repository:
    ```
    git clone https://gitlab.com/merchantsedition/efidirect.git
    ```
 2. Copy these two scripts into their intended places and ensure correct
    permissions:
    ```
    cd efidirect
    sudo mkdir -p /etc/kernel/postinst.d /etc/kernel/postrm.d
    sudo cp etc/kernel/postinst.d/zz-efidirect /etc/kernel/postinst.d/zz-efidirect
    sudo cp etc/kernel/postrm.d/efidirect /etc/kernel/postrm.d/efidirect
    sudo chown root:root /etc/kernel/postinst.d/zz-efidirect
    sudo chown root:root /etc/kernel/postrm.d/efidirect
    sudo chmod 755 /etc/kernel/postinst.d/zz-efidirect
    sudo chmod 755 /etc/kernel/postrm.d/efidirect
    ```
 3. Trigger `zz-efidirect` manually once, to get bootloader entries for
    currently installed kernels:
    ```
    sudo /etc/kernel/postinst.d/zz-efidirect
    ```
### Uninstallation

 1. Remove these two scripts:
    ```
    sudo rm /etc/kernel/postinst.d/zz-efidirect /etc/kernel/postrm.d/efidirect
    ```
 2. Remove kernel copies:
    ```
    sudo rm -r /boot/efi/EFI/debian-bookworm
                           # ^^^^^^^^^^^^^^^ Adjust to your actual OS.
    ```
 3. Remove EFIdirect related entries in EFI. How to do this largely depends on
    your hardware.


## Effects

On regular computer usage, EFIdirect is barely noticeable.

For dual-/multi-booting, one can now enter the EFI boot selection, usually by
pressing F2 during computer startup. There one can select which kernel to boot,
quite similar to how it's done in the Grub startup menu.

### Benefits

The good:

- Significantly faster boot process. No secondary bootloader like Grub, Lilo or
  rEFInd gets loaded.
- Very small disk footprint. Just about 10 kiB, rather than e.g. ~45 MiB for
  Grub2.
- Can improve the kernel's ability to initialize the computer's hardware in a
  sane way (says the Debian wiki).

The bad:

- EFI System Partition needs room for all bootable kernels, along with their
  `initrd.img`. Debian's default of 512 MiB is sufficient for 11 Debian or
  7 Ubuntu kernels. That said, autoinstallation of either removes old kernels,
  so regular users shouldn't collect more than 2 or 3 kernels at the same time.
- The user interface for boot selection of some BIOS/EFI implementations is
  a bit messy. If you plan to switch regularly, first check how this works on
  your hardware.

The ugly:

- One can find a whole lot of trouble reports about EFI's non-volatile RAM
  (NVRAM) no longer accepting variables. An
  [example](https://bugzilla.redhat.com/show_bug.cgi?id=947142). EFIdirect
  writes to EFI's NVRAM, so it could trigger this, at least in theory. Details
  in the [FAQ section](#FAQ).


## Rescue!

Messed up? Uninstalled Grub without running `zz-efidirect` at least once? It
suddenly stopped working? That's unfortunate, but recoverable.

 1. Get a rescue disk or Live-CD and boot from it.
    [Debian standard](https://cdimage.debian.org/debian-cd/current-live/) is
    sufficient, only a command line is needed. Any other Debian based LiveCD
    suits as well.
 2. Being booted into this rescue OS, use `lsblk` to find available disks.
    Example:
    ```
    $ lsblk
    NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    loop0    7:0    0 687,7M  1 loop /usr/lib/live/mount/rootfs/filesystem.squashfs
    sda      8:0    0     4G  0 disk
    ├─sda1   8:1    0   512M  0 part
    └─sda2   8:2    0     2G  0 part
    ```
    Here, `sda1` and `sda2` should look familiar. `sda1` is the EFI System
    Partition, `sda2` is the partition holding the Debian installation.
 3. Mount partitions and prepare for a `chroot`. That's the same as one of the
    traditional methods to rescue Grub. Don't confuse `sda1` and `sda2`, the
    second partition with the Debian installation gets mounted here:
    ```
    sudo mkdir -p /mnt
    sudo mount /dev/sda2 /mnt
    sudo mount -o bind /proc /mnt/proc
    sudo mount -o bind /dev /mnt/dev
    sudo mount -o bind /sys /mnt/sys
    sudo mount -o bind /sys/firmware/efi/efivars /mnt/sys/firmware/efi/efivars
    ```
 4. Change root and mount partitions, especially `/boot/efi`:
    ```
    sudo chroot /mnt /bin/bash
    mount -a
    ```
 5. At this point one can run `zz-efidirect`, which fixes things nicely:
    ```
    /etc/kernel/postinst.d/zz-efidirect
    ```


## FAQ

##### Does it work with secure boot enabled?

It should, Debian kernels are signed. Not yet tested, though.

##### Does EFIdirect support dual-booting/multi-booting?

For most practical purposes: yes.

Actually, better than Grub in some scenarios. Because there is no need to mount
and read the partition with Grub and its helper files on it, so this part can't
break.

By EFI design, all installations on a machine share the same EFI System
Partition and EFI can only load kernels/bootloaders from this partition.
Loading kernels directly means, all kernels have to be copied to this partition.

EFIdirect maintains a structure by distribution, like
```
EFI\debian-buster\
EFI\debian-bookworm\
EFI\ubuntu-focal\
```
With this, distinct distributions don't disturb each other. If you want to
maintain two installations of the same distribution, you're out of luck, though.

BTW., Grub puts its files into `EFI\debian\`, so there's no conflict either.

##### What about mounting the EFI System Partition at `/boot`?

Yes, good idea! Actually, that's how the first version of EFIdirect was
implemented, see first commit in this repository. Maintaining two copies of the
same set of kernels is a waste, after all.

But there are catches:

- No more dual-/multi-booting of distinct Linux distributions. All kernels of
  all installations would live in the same directory, keeping them sorted
  through updates would be a nightmare.
- _Dpkg_, the base tool for Debian package maintenance, doesn't like FAT32 file
  systems, while EFI insists on FAT32. Which means, kernel updates fail.
  Raspian has apparently [the same problem](https://raspberrypi.stackexchange.com/questions/51410/what-is-rpikernelhack)
  and [a solution exists](https://itectec.com/unixlinux/dpkg-replacing-files-on-a-fat-filesystem/),
  but [Debian developers call this limitation a feature](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=982554)
  and have obviously no intention to make this possible.

Other than these two catches, such a solution works just fine.

##### Now, what's the fuzz with this NVRAM thing?

Typical user space error message of a failed write to EFI's NVRAM is along the
lines of _"Could not prepare Boot variable: No space left on device"_. Easily
reproducible on a Qemu virtual machine using TianoCore v20191122 (Qemu's
preferred choice), by running `sudo /etc/kernel/postinst.d/zz-efidirect` 20 or
30 times without rebooting in between. Apparently a very old problem, still not
fixed.

It's hardly a problem in day-to-day usage, though. Writes happen on kernel
installation, only. Rebooting causes EFI to garbage-collect it's NVRAM, which
solves the problem.

Paranoid people keep Grub installed. EFIdirect never touches Grub's EFI
bootloader entry, so this can serve as a nice fallback.

Developer workaround #1 is to use a virtual machine with a disk overlay. If
something fails, simply roll back the entire hard disk.

Developer workaround #2 is to reboot after at most 50 writes to NVRAM (using
e.g. `efibootmgr`). Using `zz-efidirect` triggers two writes per kernel.


## Development

No build instructions, because there's nothing buildable :-)

For testing, one can execute these scripts manually. Rather than depending on
shell variables provided by Dpkg, code looks at what's on disk. Running these
scripts multiple times is harmless.

For testing kernel (un-)installation without messing up the system, these
realtime- or cloud-kernels are convenient. Like:
```
sudo apt-get install linux-image-5.16.0-5-cloud-amd64
sudo apt-get remove linux-image-5.16.0-5-cloud-amd64
```
Improvements and bug reports are welcome, of course. Especially when decorated
with a patch or a link to a commit. Please use the bug tracker attached to this
repository.


## References

- [List of bootloaders available for Debian](https://wiki.debian.org/BootLoader).
  [rEFInd](http://www.rodsbooks.com/refind/) is missing there, as well as
  EFIdirect.
- Article [EFIStub](https://wiki.debian.org/EFIStub) in the Debian Wiki. That's
  a typical example of these many quick hacks floating around in the internet.
- [Debian 9 mit EFISTUB booten](https://mg.guelker.eu/blog/2018/debian-efistub.html)
  (German language). That's the best description of a less hackish
  implementation of direct booting I could find before deciding to write
  EFIdirect. Big Thank You to Marvin Gülker for his inspiration, it was quite
  helpful.
- [Package maintainer scripts and hooks](https://kernel-team.pages.debian.net/kernel-handbook/ch-update-hooks.html)
  in the Debian Linux Kernel Handbook. A few words about what to do and what to
  avoid in scripts placed in `/etc/kernel`.
